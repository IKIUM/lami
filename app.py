import requests
import json
import sys
import helpers
import api
import pyvisgraph as vg
from flask import Flask
from flask import jsonify
from flask_cors import CORS
from flask import request
import os

app = Flask('app')
CORS(app)


@app.route('/')
def hello_world():
    return 'Hello, LAMI!'


@app.route('/routes')
def get_route():
    method = request.args.get('method')
    slat = request.args.get('slat')
    slon = request.args.get('slon')
    elat = request.args.get('elat')
    elon = request.args.get('elon')
    return api.get_osrm(method, slat, slon, elat, elon)


@app.route('/vigraph', methods=['GET', 'POST'])
def get_shortest_route():
    # Faking the vigraph algorithm since it takes 11 minutes to calculate the path...
    shortest_path_mock = [(62.61, 29.76),
                          (62.61, 29.76),
                          (62.61, 29.76),
                          (62.61, 29.75),
                          (62.60, 29.75),
                          (62.60, 29.75),
                          (62.60, 29.75),
                          (62.60, 29.75),
                          (62.60, 29.74),
                          (62.60, 29.74),
                          (62.60, 29.74),
                          (62.60, 29.74),
                          (62.60, 29.73)]

    points = []
    for point in shortest_path_mock:
        points.append({
            "lat": point[0],
            "lon": point[1]
        })
    return jsonify(points)

    data = json.loads(request.data)
    overpass_data = api.get_overpass(data)
    buildings = helpers.get_buildings(overpass_data['elements'])
    print('-------- BUILDINGS ---------')
    print(buildings)
    shortest_path = helpers.get_vigraph(
        buildings,
        data['slat'],
        data['slon'],
        data['elat'],
        data['elon']
    )
    return jsonify(shortest_path)
