import requests
import json
import helpers
from flask import jsonify

def get_overpass(data):
    xml = f"""<osm-script output="json">
            <union>
                <query type="node">
                <has-kv k="building"/>
                <bbox-query s="{data['south']}" w="{data['west']}" n="{data['north']}" e="{data['east']}"/>
                </query>
                <query type="way">
                    <has-kv k="building"/>
                    <bbox-query s="{data['south']}" w="{data['west']}" n="{data['north']}" e="{data['east']}"/>
                </query>
                <query type="relation">
                    <has-kv k="building"/>
                    <bbox-query s="{data['south']}" w="{data['west']}" n="{data['north']}" e="{data['east']}"/>
                </query>
            </union>
            <print mode="body"/>
            <recurse type="down"/>
            <print mode="skeleton"/>
            </osm-script>"""
    headers = {'Content-Type': 'application/xml'}
    # Couldnt find documents for this CS API, so going to use the official API instance instead of the
    # CS API which kept returning false no matter what. The official instance works fine.
    # results = requests.post("http://cs.uef.fi/mopsi_dev/overpass/api.php", data=xml, headers=headers).text
    results = requests.post("https://lz4.overpass-api.de/api/interpreter", data=xml, headers=headers).text
    data = json.loads(results)
    print('--------------- BUILDINGS ------------')
    print(data)
    return data

def get_osrm(method, slat, slon, elat, elon):
    request_type = 'get_directions'
    url = "http://cs.uef.fi/mopsi_dev/routes/server.php?param=" \
        f"{{\"request_type\":\"{request_type}\"," \
        f"\"start_lat\":\"{str(slat)}\"," \
        f"\"start_lng\":\"{str(slon)}\"," \
        f"\"end_lat\":\"{str(elat)}\"," \
        f"\"end_lng\":\"{str(elon)}\"," \
        f"\"method\":\"{method}\"}}"
    response = requests.get(url).content
    response = json.loads(requests.get(url).json())
    return jsonify(response["routes"])
