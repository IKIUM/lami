import json
import pyvisgraph as vg

def get_buildings(data):
    locations = {}
    buildings = []
    for element in data:
        if element["type"] == "node":
            locations[element["id"]] = {"lat": element["lat"], "lon": element["lon"]}
    for element in data:
        if element["type"] == "way":
            building = []
            for node in element["nodes"]:
                building.append(locations[node])
            buildings.append(building)

    return buildings

def get_vigraph(buildings, slat, slon, elat, elon):
    polygons = []
    for building in buildings:
        print('_------------------- BUILDING _----------------------')
        print(building)
        polygon = []
        for vertex in building:
            print('_------------------- VERTEX _----------------------')
            print(vertex)
            polygon.append(vg.Point(vertex["lat"], vertex["lon"]))
        polygons.append(polygon)
    graph = vg.VisGraph()
    graph.build(polygons, workers=4)
    start = vg.Point(slat, slon)
    print('_------------------- START _----------------------')
    print(start)
    end = vg.Point(elat, elon)
    print('_------------------- END _----------------------')
    print(end)
    shortest_path = graph.shortest_path(start, end)
    print('_------------------- SHORTEST _----------------------')
    print(shortest_path)
    points = []
    for point in shortest_path:
        points.append({
            "lat": point.x,
            "lon": point.y
        })
    return points